
#include "Projectile.h"

Texture *Projectile::s_pTexture = nullptr;

Projectile::Projectile()
{
	SetSpeed(500);
	SetDamage(1);
	SetDirection(-Vector2::UNIT_Y);
	SetCollisionRadius(9);

	m_drawnByLevel = true;
}

void Projectile::Update(const GameTime *pGameTime)
{
	if (IsActive())//if projectile is active
	{
		Vector2 translation = m_direction * m_speed * pGameTime->GetTimeElapsed();//Calculation for the proper movement of projectile
		TranslatePosition(translation);//Then translate to new position based on calculation above.

		Vector2 position = GetPosition(); //Gets the position of the projectile.
		Vector2 size = s_pTexture->GetSize();//applies size and applies the texture to the projectile.

		// Is the projectile off the screen?
		if (position.Y < -size.Y) Deactivate();//if the position y corrdinate is less then the size y corrdinate call deactivate method to make is active false deacivating projectile.
		else if (position.X < -size.X) Deactivate();//if the position x corrdinate is less then the size x corrdinate call deactivate method to make is active false deacivating projectile.
		else if (position.Y > Game::GetScreenHeight() + size.Y) Deactivate();//make inactive if the position of the y corrdinate is greater than the screen height plus the size of y corrdinate.
		else if (position.X > Game::GetScreenWidth() + size.X) Deactivate();// make inactive if the position of the x corrdinate is greater than the screen width plus the size of x corrdinate.
	}

	GameObject::Update(pGameTime);
}

void Projectile::Draw(SpriteBatch *pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(s_pTexture, GetPosition(), Color::White, s_pTexture->GetCenter());
	}
}

void Projectile::Activate(const Vector2 &position, bool wasShotByPlayer)
{
	m_wasShotByPlayer = wasShotByPlayer;
	SetPosition(position);

	GameObject::Activate();
}

std::string Projectile::ToString() const
{
	return ((WasShotByPlayer()) ? "Player " : "Enemy ") + GetProjectileTypeString();
}

CollisionType Projectile::GetCollisionType() const
{
	CollisionType shipType = WasShotByPlayer() ? CollisionType::PLAYER : CollisionType::ENEMY;
	return (shipType | GetProjectileType());
}