
#include "GameplayScreen.h"
#include "Level.h"
#include "Level01.h"

GameplayScreen::GameplayScreen(const int levelIndex)
{
	m_pLevel = nullptr;// m_pLevel sets it to null.
	switch (levelIndex)//switch statement for appropriate level.
	{
	case 0: m_pLevel = new Level01(); break; //switch for level one.
	}


	SetTransitionInTime(1.0f); //transition in time is equivalent to 1 sec.
	SetTransitionOutTime(0.5f);// transition out time is equivalent to 0.5sec.

	Show();//show method for showing the appropriate screen.
}

void GameplayScreen::LoadContent(ResourceManager *pResourceManager)
{
	m_pLevel->LoadContent(pResourceManager);
}

void GameplayScreen::HandleInput(const InputState *pInput)
{
	m_pLevel->HandleInput(pInput);
}

void GameplayScreen::Update(const GameTime *pGameTime)
{
	m_pLevel->Update(pGameTime);
}

void GameplayScreen::Draw(SpriteBatch *pSpriteBatch)
{
	pSpriteBatch->Begin();

	m_pLevel->Draw(pSpriteBatch);

	pSpriteBatch->End();
}
